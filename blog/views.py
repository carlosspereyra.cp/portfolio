from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
def index(request):
    return render(request, 'index.html')

def hola(request, username):
    return HttpResponse("<h1>Hola %s</h1>" % username)
def about(request):
    return render(request, 'about.html')

def prueba(request):
    return render(request, 'ejemplo.html')