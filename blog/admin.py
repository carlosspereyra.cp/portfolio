from django.contrib import admin
from .models import About, Informacion, Imagenes

# Register your models here.
admin.site.register(About)
admin.site.register(Informacion)
admin.site.register(Imagenes)