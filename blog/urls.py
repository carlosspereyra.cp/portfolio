from django.urls import path

from . import views #Para poder ejecutar las vistas las secciones

urlpatterns = [
    path('', views.index),#se deja en blanco la pagina principal
    path('hola/<str:username>', views.hola),#se deja en blanco la pagina principal
    path('about/', views.about),#se deja en blanco la pagina principal
    path('prueba/', views.prueba),#se deja en blanco la pagina principal
   

]