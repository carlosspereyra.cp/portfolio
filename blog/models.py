from django.db import models
from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, HttpResponseRedirect

# Create your models here.
class About(models.Model):
    nombre = models.CharField(max_length=200)
    descripcion = models.TextField()
    def __str__(self):
        return self.nombre + ' - ' +self.descripcion

class Informacion(models.Model): #es una tabla en RELACION con la tabla ABOUT
    titulo = models.CharField(max_length=200)
    descripcion2 = models.TextField()
    about = models.ForeignKey(About, on_delete=models.CASCADE)
    def __str__(self):
        return self.titulo + ' - ' +self.descripcion2
    #ejemplo para poder devolver valores a la vista
    def info(request):

        return render(request, "index.html")

class Imagenes(models.Model):
    nombre = models.CharField(max_length=200)
    descripcion = models.TextField()
    imagen = models.ImageField(null=True, blank=True, upload_to="images/")
    def __str__(self):
        return self.nombre + ' - ' +self.descripcion